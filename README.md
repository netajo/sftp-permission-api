# sftp-permission-api

This patch makes sftp-server to connect to a TCP server and ask for permission 
for every read or write request (before standard permissions are checked).

### New server parameters 
To use PermissionAPI, start sftp-server with following new parameters:

- `-a localhost:5000` - API server host:port
- `-s filip` - Name of the session/user
- `-k` - If you want pathnames encoded in base64 (so they can contain 
    spaces and special characters)

Server can be run using command in `~/.ssh/authorized_keys` file, eg:

`command="../mypatched/sftp-server -a localhost:5000 -s filip" ssh-rsa AAAAB.........rest..of..the..key..................`

To manualy test permission requests, you can start netcat server: `nc -l -p 5000`

### Permission request format

When the SFTP client wants to read a file, read stat, open directory, .. 
the sftp-server will send a line to api server:

`filip read /some/file`

- `filip` - session identification provided with `-s` switch
- `read` - SFTP client wants to perform read request
- `/some/file` - filename, dirname or path (possibly encoded in base64 if you set `-k` switch)

Write request are the same instead of read is write, eg:

`filip write /some/file`

### Permission allow/deny format

After each request line api server must respond (in short time) with decision.

- send character `y` or `Y` or `1` - to grant permission
- send character `n` or `N` or `0` - to deny permission

No other characters are allowed, althought newlines, tabs and spaces are ignored.

### Notes

- api server is asked before real permissions are checked on filesystem. Granting permission doesn't mean that client will really read/write file. 
- If the permission is not granted, the request is immediately cancelled with return code SSH2_FX_PERMISSION_DENIED
- If the api server disconnects during session, session will terminate to fatal() error
