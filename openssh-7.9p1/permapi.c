// PermissionAPI
// 2020, Filip Svoboda <filip.svoboda@netajo.cz>
// BSD license

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <stdarg.h>

#include "log.h"
#include "base64.h"

#include "permapi.h"

static int sockfd = 0;

void 
permapi_open_connection(const char* hostname, const unsigned int port) 
{
    verbose("sftp-server with permission API");
    struct sockaddr_in serveraddr;
    struct hostent *server;

    /* socket: create the socket */
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) {
        fatal("%s: error opening socket", __func__);
    }

    /* gethostbyname: get the server's DNS entry */
    server = gethostbyname(hostname);
    if (server == NULL) {
        fatal("%s: host not found %s", __func__, hostname);
    }

    /* build the server's Internet address */
    bzero((char *) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
	  (char *)&serveraddr.sin_addr.s_addr, server->h_length);
    serveraddr.sin_port = htons(port);

    /* connect: create a connection with the server */
    if (connect(sockfd, (struct sockaddr*) &serveraddr, sizeof(serveraddr)) < 0) {
        fatal("%s: error connecting to host", __func__);
    }
}

void
permapi_close() 
{
    close(sockfd);
    sockfd = 0;
}

void
permapi_send_info(const unsigned char *format, ...)
{
    #define BUFFSIZE 8192
    static char buff[BUFFSIZE];

    va_list args;
    int len;
    size_t wlen;
    va_start(args, format); 

    len = vsnprintf(buff, BUFFSIZE, format, args);
    if(len >= (BUFFSIZE - 1)) { // -1 because we need to add line feed
        fatal("%s: buffer is not large enough", __func__);
    }
    buff[len++] = '\n';
    wlen = write(sockfd, buff, len);
    if(wlen != (size_t) len) {
        fatal("%s: write error", __func__);
    }
    va_end(args);
}

int 
permapi_read_decision() 
{
    while(1)
    {
        char buf;
        int n = read(sockfd, &buf, 1);

        if (n < 0) {
            fatal("%s: error reading from socket", __func__);
        }
        if (n == 0) {
            fatal("%s: permission api server disconnected", __func__);
        }

        if(buf == 'y' || buf == 'Y' || buf == '1') return 1;
        if(buf == 'n' || buf == 'N' || buf == '0') return 0;
        if(buf == '\n' || buf == '\t' || buf == ' ') continue;

        // invalid input - ignore
        ;
    }
}

int
permapi_allowed(char* action, char* path)
{
    size_t enc_path_len;
    char* enc_path;
    
    if(permapi_encode_path) {
        enc_path = base64_encode(path, strlen(path), &enc_path_len);
        if(enc_path == NULL) {
            fatal("%s: path encoding failed", __func__);
        }
    } else {
        enc_path = path;
    }

    permapi_send_info("%s %s %s", permapi_session, action, enc_path);

    if(permapi_encode_path) {
        free(enc_path);
    }
    return permapi_read_decision();
}

int
permapi_readable(char* path)
{
    return permapi_allowed("read", path);
}

int
permapi_writable(char* path)
{
    return permapi_allowed("write", path);
}


