// PermissionAPI
// 2020, Filip Svoboda <filip.svoboda@netajo.cz>
// BSD license

#ifndef PERMAPI_H
#define PERMAPI_H

extern int permapi_encode_path;
extern char *permapi_session;

void permapi_open_connection(const char* hostname, const unsigned int port);
void permapi_close();

int permapi_readable(char* name);
int permapi_writable(char* name);

#endif
